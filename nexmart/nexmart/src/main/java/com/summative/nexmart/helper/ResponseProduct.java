package com.summative.nexmart.helper;

import java.util.List;

import com.summative.nexmart.entity.Product;

public class ResponseProduct {
	private int status;
	private String message;
	private List<Product> data;

	public ResponseProduct(int status, String message, List<Product> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Product> getData() {
		return data;
	}

	public void setData(List<Product> data) {
		this.data = data;
	}
}
