package com.summative.nexmart.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

@Entity
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String product_name;
	private int price;
	private String category;
	private int stock;
	private LocalDateTime created_at;
	
	@OneToMany(mappedBy = "product")
	List<Transaction> transaction = new ArrayList<>();
//	List<Junction> junctions;

	public Product() {
		super();
	}

	public Product(String product_name, int price, String category, int stock, LocalDateTime created_at) {
		super();
		this.product_name = product_name;
		this.price = price;
		this.category = category;
		this.stock = stock;
		this.created_at = created_at;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public LocalDateTime getCreated_at() {
		return created_at;
	}

	 @PrePersist
	public void setCreated_at() {
		this.created_at = LocalDateTime.now();
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", product_name=" + product_name + ", price=" + price + ", category=" + category
				+ ", stock=" + stock + ", created_at=" + created_at + "]";
	}
	
}
