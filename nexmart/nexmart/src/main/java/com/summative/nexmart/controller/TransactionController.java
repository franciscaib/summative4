package com.summative.nexmart.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.summative.nexmart.entity.Product;
import com.summative.nexmart.entity.Transaction;
import com.summative.nexmart.helper.ResponseProduct;
import com.summative.nexmart.helper.ResponseTransaction;
import com.summative.nexmart.service.TransactionRepository;
import com.summative.nexmart.service.productRepository;

@RestController
public class TransactionController {
	
	@Autowired
	TransactionRepository transactionRep;
	
	@Autowired
	productRepository productRep;
	
	@GetMapping(value = "/api/payments", produces = "application/json")
	public ResponseEntity<ResponseTransaction> getAllTransaction () {
		List<Transaction> transaction = transactionRep.findAll();
		if(transaction.isEmpty()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseTransaction(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseTransaction(HttpStatus.OK.value(), 
						"Data berhasil ditemukan", transaction));
	}
		
	@PostMapping("/api/payment")
	public ResponseEntity<ResponseTransaction> postTransaction(@RequestBody Transaction transaction) {
		if(transaction.getProduct().getId() == 0) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseTransaction(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Tidak ada product", null));
		}
		transaction.setCreated_at();
		int price = productRep.getPrice(Long.valueOf(transaction.getProduct().getId()));
		transaction.setTotal_price(price * transaction.getQuantity());
		int stock = productRep.getStock(Long.valueOf(transaction.getProduct().getId()));
		int qty = stock-transaction.getQuantity();
		productRep.update(qty,Long.valueOf(transaction.getProduct().getId()));
		Optional<Product> pro = productRep.findById(Long.valueOf(transaction.getProduct().getId()));
		transactionRep.insertTransactional(transaction.getProduct().getId(), transaction.getQuantity(),
				transaction.getCreated_at());
		Transaction tran = transactionRep.getLastTransaction();
		tran.setProduct(pro.get());
		transactionRep.save(tran);
		List<Transaction> tr = new ArrayList<>();
		tr.add(tran);
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(new ResponseTransaction(HttpStatus.CREATED.value(), 
						"Data berhasil dibuat", tr));
	}	
	@PostMapping("/api/payment/search")
	public ResponseEntity<ResponseTransaction> searchTransactionDaily 
	(@RequestBody Transaction transaction){
		List<Transaction> tr = new ArrayList<>();
		tr = transactionRep.searchTransactionDaily(transaction.getCreated_at());
		if(tr.isEmpty()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseTransaction(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseTransaction(HttpStatus.OK.value(), 
						"Data berhasil ditemukan", tr));
	}
}
