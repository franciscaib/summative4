package com.summative.nexmart.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.summative.nexmart.entity.Transaction;

public interface TransactionRepository  extends JpaRepository<Transaction, Long>{

	@Query(value = "SELECT * FROM Transaction WHERE DATE(created_at) = ?1",
			nativeQuery = true)
	List<Transaction> searchTransactionDaily (String date);
	
	@Transactional
	@Modifying
	@Query(value = "INSERT INTO Transaction(product_id,total_price, quantity,created_at) VALUES "
			+ "(?1,0, ?2,?3);",
			nativeQuery = true)
	int insertTransactional(Long product_id, int quantity, String tanggal);
	
	@Query(value = "SELECT * FROM transaction WHERE id=(SELECT LAST_INSERT_ID())", nativeQuery = true)
	Transaction getLastTransaction();
}
