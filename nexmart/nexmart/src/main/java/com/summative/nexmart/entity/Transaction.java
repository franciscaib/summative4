package com.summative.nexmart.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

@Entity
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int total_price;
	private int quantity;
	private String created_at;

	@ManyToOne
	Product product;

	public Transaction() {
		super();
	}

	public Transaction(int total_price, int quantity, String created_at) {
		super();
		this.total_price = total_price;
		this.quantity = quantity;
		this.created_at = created_at;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTotal_price() {
		return total_price;
	}

	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getCreated_at() {
		return created_at;
	}

	@PrePersist
	public void setCreated_at() {
		String date = LocalDateTime.now().toString();
		this.created_at = date;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", total_price=" + total_price + ", quantity=" + quantity + ", created_at="
				+ created_at + ", product=" + product + "]";
	}
	
}
