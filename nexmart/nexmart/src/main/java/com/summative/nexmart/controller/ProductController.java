package com.summative.nexmart.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.summative.nexmart.entity.Product;
import com.summative.nexmart.helper.ResponseProduct;
import com.summative.nexmart.service.productRepository;

@RestController
public class ProductController {

	@Autowired
	productRepository productRep;
	private int stock;
	private String product_name;
	private String category;
	private String product_name2;
	
	@GetMapping(value = "/api/products", produces = "application/json")
	public ResponseEntity<ResponseProduct> getAllData(){
		List<Product> products = productRep.findAll();
		if(products.isEmpty()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseProduct(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseProduct(HttpStatus.OK.value(), 
						"Data berhasil ditemukan", products));
	}

	@PostMapping("/api/product")
	public ResponseEntity<ResponseProduct> postNewData(@RequestBody List<Product> products) {
		if(products.isEmpty()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseProduct(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		productRep.saveAll(products);
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(new ResponseProduct(HttpStatus.CREATED.value(), 
						"Data berhasil dibuat", products));
	}
	
	@PutMapping(value = "/api/product/{id}", 
			produces = "application/json")
	public ResponseEntity<ResponseProduct> editData(@PathVariable String id,
			@RequestBody Product pro) {
		Optional<Product> product = null;
		product =productRep.findById(Long.valueOf(id));
		if(product == null ) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseProduct(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		product.get().setProduct_name(pro.getProduct_name());
		product.get().setPrice(pro.getPrice());
		product.get().setCategory(pro.getCategory());
		product.get().setStock(pro.getStock());
		productRep.save(product.get());
		List<Product> p = new ArrayList<>();
		p.add(product.get());
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseProduct(HttpStatus.OK.value(), 
						"Data berhasil diubah", p));
	}

	
	@DeleteMapping("/api/product/{id}")
	public ResponseEntity<ResponseProduct> deleteData(@PathVariable int id) {
		Optional<Product> product = null;
		product =productRep.findById(Long.valueOf(id));
		if(product == null) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseProduct(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		productRep.deleteById(Long.valueOf(id));
		List<Product> p = new ArrayList<>();
		p.add(product.get());
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseProduct(HttpStatus.OK.value(), 
						"Data berhasil dihapus", p));
	}
	
	@PostMapping("/api/product/search")
	public ResponseEntity<ResponseProduct> searchData(@RequestParam int stock, @RequestParam String product_name,
			@RequestParam String category){
		List<Product> p =  productRep.findAll();
		List<Product> pr = new ArrayList<>();
		for (Product product : p) {
			
			if(product.getStock() == stock && product.getProduct_name().equals(product_name)
					&& product.getCategory().equals(category)) {
				pr.add(product);
			}
		}
		if(pr.isEmpty()) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(new ResponseProduct(HttpStatus.INTERNAL_SERVER_ERROR.value(),
							"Data tidak ditemukan", null));
		}
		return ResponseEntity.status(HttpStatus.OK)
				.body(new ResponseProduct(HttpStatus.OK.value(), 
						"Data berhasil dihapus", pr));
	}

}
