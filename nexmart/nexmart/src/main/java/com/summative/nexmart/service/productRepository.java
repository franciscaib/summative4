package com.summative.nexmart.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import com.summative.nexmart.entity.Product;

public interface productRepository  extends JpaRepository<Product, Long>{
	
	@Query(value = "SELECT * FROM Product WHERE stock = ?1 AND product_name = ?2"
			+ "AND category = ?3;",
			nativeQuery = true)
	Product searchData(int stock, String product_name, String category);
	
	@Query(value = "select price from Product WHERE id = ?1", nativeQuery = true)
	int getPrice(Long product_id);
	
	@Transactional
	@Modifying
	@Query(value ="UPDATE Product SET stock = ?1 WHERE id=?2", nativeQuery = true)
	int update(int stock, Long product_id);
	
	@Query(value="SELECT stock FROM Product WHERE id= ?1", nativeQuery = true)
	int getStock(Long product_id);
}
