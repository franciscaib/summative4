package com.summative.nexmart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NexmartApplication {

	public static void main(String[] args) {
		SpringApplication.run(NexmartApplication.class, args);
	}

}
