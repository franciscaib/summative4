package com.summative.nexmart.helper;

import java.util.List;

import com.summative.nexmart.entity.Transaction;

public class ResponseTransaction {
	private int status;
	private String message;
	private List<Transaction> data;

	public ResponseTransaction(int status, String message, List<Transaction> data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public List<Transaction> getData() {
		return data;
	}

	public void setData(List<Transaction> data) {
		this.data = data;
	}
}

