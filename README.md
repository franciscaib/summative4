**SUMMATIVE 4**

| Method              | Endpoint             |
| ------------------- | -------------------- |
| getAllData [Get]    | /api/products        |
| postNewData [Post]  | /api/product         |
| editData [Put]      | /api/product/{id}    |
| deleteData [Delete] | /api/product/{id}    |
| searchData[Post]    | /api/product/search? |

**Success response**

**Product**

**----------**

**getAllData**

![](./Gambar/successGetAllDataProduct.JPG)

postNewData

![](./Gambar/successPostNewDataProduct.JPG)

editData

![](./Gambar/successEditDataProduct.JPG)

DeleteData

![](./Gambar/successDeleteDataProduct.JPG)

SearchProduct

![](./Gambar/successSearchDataProduct.JPG)

| Method                        | Endpoint             |
| ----------------------------- | -------------------- |
| getAllTransaction [Get]       | /api/payments        |
| postTransaction [Post]        | /api/payment         |
| searchTransactionDaily [Post] | /api/payment/search? |

**Transaction**

getAllTransaction

![](./Gambar/successGetAllTransaction.JPG)

postTransaction

![](./Gambar/successPostNewTransaction.JPG)

searchTransactionDaily

![](./Gambar/successSearchTransactionDaily.JPG)



**INTERNAL_SERVER_ERROR Response**

![](./Gambar/dataSearchNotFound.JPG)

